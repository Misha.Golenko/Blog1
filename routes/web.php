<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function (){
    Route::get('/', 'BasicController@index');
    Route::get('/category/{id}', 'CategoryViewController@index');
    Route::get('/article/{id}', 'ArticleViewController@index');
    Route::get('/tag/{id}', 'TagViewController@index');
    Route::post('/search', 'SearchController@search');
    Route::get('/search', 'SearchController@search');
    Route::auth();
});

Route::group(['prefix'=>'admin', 'middleware'=>'auth'], function (){
   Route::get('/', function (){
        return view('layouts.layout_admin');
   });
   Route::resource('article', 'ArticleController');
   Route::resource('category', 'CategoryController');
   Route::resource('tag', 'TagController');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

