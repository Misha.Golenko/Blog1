<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use App\ContentArticle;

class ArticleViewController extends Controller
{
    public function index($id){
        $tag_active = Tag::where('is_active', 1)->get();
        $category_active = Category::where('is_active', 1)->get();
        $articles_is_active = Article::where('is_active', 1)->get();

        $article_this = Article::find($id);
        $article_this_contents = $article_this->content()->orderBy('is_one_in_row')->get();
        $article_this_tags = $article_this->tags()->get();
        return view('article',['categories'=> $category_active, 'tags'=>$tag_active,
            'articles_is_active'=>$articles_is_active, 'article_this'=>$article_this,
            'article_this_contents'=>$article_this_contents, 'article_this_tags'=>$article_this_tags]);

    }
}
