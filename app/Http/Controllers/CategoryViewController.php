<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use App\ContentArticle;

class CategoryViewController extends Controller
{
    public function index($id){
        $tag_active = Tag::where('is_active', 1)->get();
        $category_active = Category::where('is_active', 1)->get();
        $articles_is_active = Article::where('is_active', 1)->get();

        $category_this = Category::find($id);
        $category_this_articles = $category_this->articles()->latest()->Paginate(3);

        return view('category',['categories'=> $category_active, 'tags'=>$tag_active,
            'articles_is_active'=>$articles_is_active, 'category_this'=>$category_this,'category_this_articles'=>$category_this_articles]);

    }
}
