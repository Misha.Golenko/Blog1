<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\ContentArticle;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('admin.article',['articles'=> $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = Article::with('tags','categories')->find(7);
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.article_create', ['categories'=> $categories, 'tags'=> $tags, 'article'=> $article]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $article = new Article();
        $article->is_active = $request->is_active??0;
        $article->is_show_slider = $request->is_show_slider??0;
        $article->title = $request->title;
        $article->description = $request->description;

        if($request->hasFile('image')){
            $file = $request->file('image');
            $article->image = $file->getClientOriginalName();
            $file->move(public_path().'/images',$article->image);
        }
        $article->save();

            foreach ($request->content_art as $cont) {
                if (isset($cont['type'])) {
                    if ($cont['type'] == 'img') {
                        $cont['value']->move(public_path() . '/images', $cont['value']->getClientOriginalName());
                        $content = $cont['value']->getClientOriginalName();
                        $data = [
                            'type' => $cont['type'],
                            'content' => $content,
                            'is_one_in_row' => isset($cont['is_one_in_row'])
                        ];
                    } else {
                        $data = [
                            'type' => $cont['type'],
                            'content' => $cont['value'],
                            'is_one_in_row' => isset($cont['is_one_in_row'])
                        ];
                    }



                $article->content()->create($data);
                }
            }

        $tag = $request->tag??[];
        $article->tags()->sync($tag);

        $category = $request->category_id;
        $article->categories()->sync($category);


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $contents = $article->content;
        //dd($contents);
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.article_edit',['categories'=> $categories, 'tags'=> $tags,
            'article'=> $article,'contents'=>$contents]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->is_active = $request->is_active??0;
        $article->is_show_slider = $request->is_show_slider??0;
        $article->title = $request->title;
        $article->description = $request->description;

        if($request->hasFile('image')){
            $file = $request->file('image');
            $article->image = $file->getClientOriginalName();
            $file->move(public_path().'/images',$article->image);
        }

        $article->save();

        $article->content()->delete();

        foreach ($request->content_art as $cont) {
            if($cont['type'] =='img'){
                if(isset($cont['value'])) {
                    $cont['value']->move(public_path() . '/images', $cont['value']->getClientOriginalName());
                    $content = $cont['value']->getClientOriginalName();
                }else{
                    $content = $cont['old_cont'];
                }
                $data = [
                    'type' => $cont['type'],
                    'content' => $content,
                    'is_one_in_row' => isset($cont['is_one_in_row'])
                ];
            }else{
                $data = [
                    'type' => $cont['type'],
                    'content' => $cont['value'],
                    'is_one_in_row' => isset($cont['is_one_in_row'])
                ];
            }


            $article->content()->create($data);
        }


        $tag = $request->tag??[];
        $article->tags()->sync($tag);

        $category = $request->category_id;
        $article->categories()->sync($category);


        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
        return back();
    }
}
