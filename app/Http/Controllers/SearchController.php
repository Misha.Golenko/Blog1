<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use App\ContentArticle;

class SearchController extends Controller
{
    public function index($id){


    }

    public function search(Request $request){
        $tag_active = Tag::where('is_active', 1)->get();
        $category_active = Category::where('is_active', 1)->get();
        $articles_is_active = Article::where('is_active', 1)->get();

        $search = $request->search;
        $search_articles =
            Article::whereHas('categories', function ($query) use($search){
                $query->where('name', 'like', "%$search%");
            })
                ->orwhereHas('tags', function ($query) use($search){
                    $query->where('name', 'like', "%$search%");
                })
                ->orWhere('title','LIKE',"%$search%")
                ->orWhere('description','LIKE',"%$search%")
                ->orwhereHas('content', function ($query) use($search){
                    $query->where('content', 'like', "%$search%");
                })
                ->where('is_active', 1)->latest()->paginate(3);

        return view('search',['categories'=> $category_active, 'tags'=>$tag_active,
            'articles_is_active'=>$articles_is_active, 'search_articles'=>$search_articles, 'request_search'=>$request->search]);
    }
}
