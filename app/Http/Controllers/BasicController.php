<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use App\ContentArticle;

class BasicController extends Controller
{
    public function index()
    {
            $tag_active = Tag::where('is_active', 1)->get();
            $category_active = Category::where('is_active', 1)->get();
            $articles_is_show_slider = Article::where('is_show_slider', 1)->get();
            $articles_is_active = Article::where('is_active', 1)->latest()->Paginate(3);

    return view('home', ['categories'=> $category_active, 'tags'=>$tag_active,'articles_is_show_slider'=>$articles_is_show_slider,'articles_is_active'=>$articles_is_active]);

    }
}
