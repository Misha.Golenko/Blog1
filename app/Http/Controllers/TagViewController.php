<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use App\ContentArticle;

class TagViewController extends Controller
{
    public function index($id){
        $tag_active = Tag::where('is_active', 1)->get();
        $category_active = Category::where('is_active', 1)->get();
        $articles_is_active = Article::where('is_active', 1)->get();

        $tag_this = Tag::find($id);
        $tag_this_articles = $tag_this->articles()->latest()->Paginate(3);

        return view('tag',['categories'=> $category_active, 'tags'=>$tag_active,
            'articles_is_active'=>$articles_is_active, 'tag_this'=>$tag_this,'tag_this_articles'=>$tag_this_articles]);

    }
}
