<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentArticle extends Model
{
    protected $fillable = ['type','content','is_one_in_row','article_id'];

    protected $table = 'article_contents';

    public function article(){
        return $this->belongsTo(Article::class);
    }
}
