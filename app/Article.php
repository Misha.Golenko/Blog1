<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $guarded = [];
    public function content(){
        return $this->hasMany(ContentArticle::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'category_articles');
    }
    public function tags(){
        return $this->belongsToMany(Tag::class, 'tag_articles');
    }
}
