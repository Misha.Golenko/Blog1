@extends ('layouts.layout_admin')
@section('content')
    <form method="post" action="{{action('TagController@update',['id' => $tag->id])}}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        <div class="form-group">
            <label>{{$tag->name}}</label>
            <input type="text" name="name" class="form-control" placeholder="Тег" value="{{$tag->name}}">
        </div>
        <div class="checkbox">
            <label>
                <input name="is_active" value="1" type="checkbox" {{ $tag->is_active!==0?"checked":""}}> Показывать тег
            </label>
        </div>
        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>
@endsection