@extends ('layouts.layout_admin')
@section('content')
    <form method="post" action="{{action('CategoryController@store')}}" enctype="multipart/form-data">
        <div class="form-group">
            <label>Новая категория</label>
            <input type="text" name="name" class="form-control" placeholder="Категория">
        </div>
        <div class="checkbox">
            <label>
                <input name="is_active" value="1" type="checkbox"> Показывать категорию
            </label>
        </div>
        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>
@endsection