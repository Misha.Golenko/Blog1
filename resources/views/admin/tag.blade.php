@extends ('layouts.layout_admin')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-2 col-md-pull-3">
                    <a href="{{action('TagController@create')}}" class="btn btn-default">Создать тег</a>
                </div>
                <div class="col-md-4 col-md-push-3">
                    <h2>Теги</h2>
                </div>
            </div>
            <ul class="list-group">
                @foreach($tags as $tag)
                    <div style="margin: 20px">
                    <li class="list-group-item" style="float: left; width: 650px ">{{$tag->name}}</li>
                    <form style="display: inline" action="{{action('TagController@destroy',['id' => $tag->id])}}" method="POST">
                        {{ method_field('DELETE') }}
                        <button style="" class="btn btn-default" type="submit">Удалить</button>
                    </form>
                    <form style="display: inline" action="{{action('TagController@edit',['id' => $tag->id])}}" method="GET">
                        <button style="" class="btn btn-default" type="submit">Редактировать</button>
                    </form>
                    </div>
                @endforeach
            </ul>


        </div>
    </div>
@endsection