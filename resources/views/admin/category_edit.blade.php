@extends ('layouts.layout_admin')
@section('content')
    <form method="post" action="{{action('CategoryController@update',['id'=> $category->id])}}" enctype="multipart/form-data">
        {{ method_field('PATCH') }}
        <div class="form-group">
            <label>Категория {{$category->name}}</label>
            <input type="text" name="name" class="form-control" placeholder="Категория" value="{{$category->name}}">
        </div>
        <div class="checkbox">
            <label>
                <input name="is_active" value="1" type="checkbox" {{ $category->is_active!==0?"checked":""}}> Показывать категорию
            </label>
        </div>
        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>
@endsection