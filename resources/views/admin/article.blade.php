@extends ('layouts.layout_admin')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-2 col-md-pull-3">
                <a href="{{action('ArticleController@create')}}" class="btn btn-default">Создать статью</a>
            </div>
            <div class="col-md-4 col-md-push-3">
                <h2>Статьи</h2>
            </div>
        </div>
        <table class="table table-striped ">
            <thead>
            <tr>
                <th>Название статьи</th>
                <th>Краткое описание</th>
                <th>Главная картинка</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($articles))
            @foreach($articles as $article)
            <tr>
                <td>{{$article->title}}</td>
                <td>{{$article->description}}</td>
                <td><img style="width: 140px; height: 90px" src='{{asset("images/$article->image")}}' alt="Нет картинки"></td>
                <td>
                    <form style="display: inline" action="{{action('ArticleController@destroy',['id' => $article->id])}}" method="POST">
                        {{ method_field('DELETE') }}
                        <button style="" class="btn btn-default" type="submit">Удалить</button>
                    </form>
                </td>
                <td>
                    <form style="display: inline" action="{{action('ArticleController@edit',['id' => $article->id])}}" method="GET">
                        <button style="" class="btn btn-default" type="submit">Редактировать</button>
                    </form>
                </td>
            </tr>
            @endforeach
                @endif
            </tbody>
        </table>



    </div>
</div>
@endsection