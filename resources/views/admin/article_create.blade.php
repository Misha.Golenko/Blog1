@extends ('layouts.layout_admin')
@section('content')
    <form method="post" action="{{action('ArticleController@store')}}" enctype="multipart/form-data">
        <div class="form-group">
            <label>Заголовок статьи</label>
            <input type="text" name="title" class="form-control" placeholder="Заголовок" required>
        </div>
        <div class="form-group">
            <label >Добавить главное изображение</label>
            <input class="" type="file" name="image" required>
        </div>
        <div class="form-group">
            <label>Короткое содержание</label>
            <input type="text" name="description" class="form-control" placeholder="Короткое содержание" required>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Контент</h3>
            </div>
            <div class="panel-body">

        <button class="click_file btn btn-default">Добавить картинку</button>
        <button class="click_text btn btn-default">Добавить текст</button>
        <div style="padding: 10px" class="cont">

        </div>
            </div>
        </div>

        <div class="checkbox">
            <label>
                <input name="is_show_slider" value="1" type="checkbox"> Показывать статью в слайдере
            </label>
        </div>
        <div class="checkbox">
            <label>
                <input name="is_active" value="1" type="checkbox"> Показывать статью
            </label>
        </div>
        <p>Выбор категории</p>
        <select name="category_id" style="margin: 15px; width: 150px; height: 50px">
            @foreach($categories as $category)
                <option name="category" value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select></br>
        @foreach($tags as $tag)
            <div style="float: left; margin: 15px">
            <p>{{$tag->name}}</p>
            <input name="tag[]" value="{{$tag->id}}" type="checkbox">
            </div>
        @endforeach



        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>
    <script>
        var index = 0;

        $('.click_file').on('click', function (event) {
            event.preventDefault();
            ++index;
            $('.cont').append('<div class="panel panel-body"><label>Добавление картинки</label><input class="cont_file" type="file" name="content_art['+index+'][value]"><div class="checkbox">' +
                '<input type="hidden" name="content_art['+index+'][type]" value="img">\n' +
                '            <label>\n' +
                '                <input name="content_art['+index+'][is_one_in_row]" value="1" type="checkbox"> На 50%\n' +
                '            </label>\n' +
                '        </div><button class="rm_cont">Удалить</button> </div>');

        });

        $('.click_text').on('click', function (event) {
            event.preventDefault();
            ++index;
            $('.cont').append('<div class="panel panel-body"><label>Добавление текста</label><textarea id="ckeditor'+index+'" class="cont_file ckeditor"  name="content_art['+index+'][value]"></textarea>' +
                '<input type="hidden" name="content_art['+index+'][type]" value="text">' +
                '<div class="checkbox">\n' +
                '            <label>\n' +
                '                <input name="content_art['+index+'][is_one_in_row]" value="1" type="checkbox"> На 50%\n' +
                '            </label>\n' +
                '        </div><button class="rm_cont">Удалить</button> </div>');
            CKEDITOR.replace( 'ckeditor'+index );

        });

        $(document).on('click','.rm_cont',function (event) {
            event.preventDefault();
            $(this).parent().remove();
        })

    </script>
@endsection