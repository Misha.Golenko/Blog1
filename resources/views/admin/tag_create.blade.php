@extends ('layouts.layout_admin')
@section('content')
    <form method="post" action="{{action('TagController@store')}}" enctype="multipart/form-data">
        <div class="form-group">
            <label>Новый тег</label>
            <input type="text" name="name" class="form-control" placeholder="Тег">
        </div>
        <div class="checkbox">
            <label>
                <input name="is_active" value="1" type="checkbox"> Показывать статью
            </label>
        </div>
        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>
@endsection