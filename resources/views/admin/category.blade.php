@extends ('layouts.layout_admin')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-2 col-md-pull-3">
                    <a href="{{action('CategoryController@create')}}" class="btn btn-default">Создать категорию</a>
                </div>
                <div class="col-md-4 col-md-push-1">
                    <h2>Категории</h2>
                </div>
            </div>
            <ul class="list-group">
                @foreach($categories as $category)
                    <div style="margin: 20px">
                <li class="list-group-item" >{{$category->name}}</li>
                    <form style="display: inline" action="{{action('CategoryController@destroy',['id' => $category->id])}}" method="POST">
                        {{ method_field('DELETE') }}
                        <button style="" class="btn btn-default" type="submit">Удалить</button>
                    </form>
                    <form style="display: inline" action="{{action('CategoryController@edit',['id' => $category->id])}}" method="GET">
                        <button style="" class="btn btn-default" type="submit">Редактировать</button>
                    </form>
                    </div>
                @endforeach
            </ul>


        </div>
    </div>
@endsection
