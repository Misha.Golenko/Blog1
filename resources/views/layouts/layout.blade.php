<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <style type="text/css">
        h2{
            margin: 0;
            color: black;
            padding-top: 90px;
            font-size: 52px;
            font-family: "trebuchet ms", sans-serif;
        }
        .item{
            background: #555;
            text-align: center;
            height: 300px !important;
            position: relative;
        }
        .carousel{
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .nav{
            margin-bottom: 15px;
        }
        .slider_img{
            width: 600px;
            height: 300px !important;
            position: absolute;
            top: 0;
            left: 50%;
            margin-left:-300px;
            z-index: -1;
        }
        .con{
            padding: 10px;
        }
    </style>

    <title>Document</title>

</head>
<body>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                 @section('sidebar')
            <ul class="nav nav-pills">
                <li role="presentation" class="active"><a class="ajaxLoad" href="/">Главная</a></li>
                @if(isset($categories) && count($categories)>0)
                @if(count($categories) <= 2)
                    @foreach($categories as $k => $category)
                        <li role="presentation"><a class="ajaxLoad" href="{{action('CategoryViewController@index',['id'=>$categories[0]->id])}}">{{$categories[0]->name}}</a></li>
                    @endforeach
                            @else
                        <li role="presentation" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                Категории <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                    @foreach($categories as $k => $category)
                                        <li><a class="ajaxLoad" href="{{action('CategoryViewController@index',['id'=>$category->id])}}">{{$category->name}}</a></li>
                                    @endforeach
                            </ul>
                        </li>

                    @endif
                @endif
                @if(isset($tags) && sizeof($tags))
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        Теги <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach($tags as $tag)
                        <li><a class="ajaxLoad" href="{{action('TagViewController@index',['id'=>$tag->id])}}">{{$tag->name}}</a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>
                     @endif
        </div>
        @show
        @section('slider')
        @show

            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-4">
                    <div class="col-md-12">
                        @if(isset($tags) && sizeof($tags))
                        <div class="list-group">
                            <h3 class="list-group-item">Теги</h3>
                            @foreach($tags as $tag)
                                <a href="{{action('TagViewController@index',['id'=>$tag->id])}}" class="list-group-item ajaxLoad">{{$tag->name}}</a>
                            @endforeach
                        </div>
                            @endif
                    </div>
                    <div class="col-md-12">
                        @if(isset($categories) && sizeof($categories))
                        <div class="list-group">
                            <h3 class="list-group-item">Категории</h3>
                        @foreach($categories as $category)
                                <a href="{{action('CategoryViewController@index',['id'=>$category->id])}}" class="list-group-item ajaxLoad">{{$category->name}}</a>
                            @endforeach
                        </div>
                            @endif
                    </div>
                    <div class="col-md-12">
                        <form class="navbar-form navbar-left" method="post" action="{{action('SearchController@search')}}">
                            <div class="form-group">
                                <input name="search" type="text" class="form-control" placeholder="Поиск">
                            </div>
                            <button type="submit" class="btn btn-default">Поиск</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-8" id="main-content">
                @section('content')
                    @show
                </div>
            </div>

                     @section('footer')
                         <div class="col-md-8 col-md-offset-2">
                         <div class="panel panel-default">
                             <div class="panel-body">
                                 <div class="col-md-4">
                                     @if(isset($tags) && sizeof($tags))
                                     <div class="list-group">
                                         <h3 class="list-group-item">Теги</h3>
                                     @foreach($tags as $tag)
                                         <a href="{{action('TagViewController@index',['id'=>$tag->id])}}" class="ajaxLoad list-group-item">{{$tag->name}}</a>
                                         @endforeach
                                     </div>
                                         @endif
                                 </div>
                                 <div class="col-md-4">
                                     @if(isset($categories) && sizeof($categories))
                                     <div class="list-group">
                                         <h3 class="list-group-item">Категории</h3>
                                     @foreach($categories as $category)
                                             <a href="{{action('CategoryViewController@index',['id'=>$category->id])}}" class="list-group-item ajaxLoad">{{$category->name}}</a>
                                         @endforeach
                                     </div>
                                         @endif
                                 </div>
                                 <div class="col-md-4">
                                         <form class="navbar-form navbar-left" method="post" action="{{action('SearchController@search')}}">
                                             <div class="form-group">
                                                 <input name="search" type="text" class="form-control" placeholder="Поиск">
                                             </div>
                                             <button type="submit" class="btn btn-default">Поиск</button>
                                         </form>
                                 </div>
                             </div>
                             <div class="panel-footer text-center">Panel footer</div>
                         </div>
                     @show
        </div>
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    $(document).on('click','.ajaxLoad',function (event) {
        event.preventDefault();
        history.pushState('','',$(this).attr('href'))
        $.ajax({url:$(this).attr('href')}).success(
            function(data){
                $( 'html' ).html( data );
            }).error();

    });
</script>
</body>
</html>