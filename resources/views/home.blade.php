@extends('layouts.layout')
@section('slider')
    @if(isset($articles_is_show_slider) && sizeof($articles_is_show_slider))
    <div id="myCarousel" class="carousel slide" data-interval="" data-ride="carousel">
        <!-- Слайды карусели -->
        <div class="carousel-inner">

            @foreach($articles_is_show_slider as $k => $article_is_show_slider)
                @if($k == 0)
                    <div class="item active">
                @else
                            <div class="item">
                @endif
                    <h2>{{$article_is_show_slider->title}}</h2>
                    <img class="slider_img" src='{{asset("images/$article_is_show_slider->image")}}'>
                    <div class="carousel-caption">
                        <p style="color: black">{{$article_is_show_slider->description}}</p>
                        <a href="{{action('ArticleViewController@index',['id'=>$article_is_show_slider->id])}}" class="btn btn-default ajaxLoad">Читать далее</a>
                    </div>
                </div>
        @endforeach
        </div>
        <!-- Навигация для карусели -->
        <!-- Кнопка, осуществляющая переход на предыдущий слайд с помощью атрибута data-slide="prev" -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <!-- Кнопка, осуществляющая переход на следующий слайд с помощью атрибута data-slide="next" -->
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
        @endif
@endsection
@section('content')
            <h3 style="text-align: center">главная страница</h3>
            <div class="row">
                @foreach($articles_is_active as $article_is_active)
                    <a class="ajaxLoad" href="{{action('ArticleViewController@index',['id'=>$article_is_active->id])}}">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <img style="width: 100%; height: auto" class="" src='{{asset("images/$article_is_active->image")}}'>
                        </div>
                        <div class="panel-body">
                            <h4>{{$article_is_active->title}}</h4>
                        </div>
                        <div class="panel-body">
                            <p>{{$article_is_active->description}}</p>
                        </div>
                        <div class="panel-body">
                            <p>{{$article_is_active->created_at->format('d-m-Y')}}</p>
                        </div>
                    </div>
                </div>
                    </a>
                @endforeach

            </div>
            <div class="col-md-offset-4">{{$articles_is_active->links()}}</div>

@endsection
