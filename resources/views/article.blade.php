@extends('layouts.layout')
@section('content')
    <h1 style="text-align: center">{{$article_this->title}}</h1>
    <img style="width: 100%; height: auto" class="" src='{{asset("images/$article_this->image")}}'>
        @if(isset($article_this_contents) && count($article_this_contents)>0)

            @foreach($article_this_contents as $article_this_content)
                @if($article_this_content->is_one_in_row == 1)
                    <div class="con col-md-6 col-sm-12">
                        @if($article_this_content->type == 'img')
                            <img style="width: 100%; height: auto" src='{{asset("images/$article_this_content->content")}}' alt="">
                        @else
                            <div class="">{{$article_this_content->content}}</div>
                        @endif
                    </div>
                @else
                    <div class="con col-md-12">
                        @if($article_this_content->type == 'img')
                            <img style="width: 100%; height: auto" src='{{asset("images/$article_this_content->content")}}' alt="">
                        @else
                            <div class="">{!! $article_this_content->content !!}</div>
                        @endif
                    </div>
                @endif
            @endforeach

        @endif

    <div style="margin: 15px" class="row">
        <div class="col-md-12">
        @if(isset($article_this_tags) && count($article_this_tags)>0)
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        @foreach($article_this_tags as $article_this_tag)
                            <div style="padding: 5px" class="col-md-4">
                                <div class="well">{{$article_this_tag->name}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
            <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="well">{{$article_this->created_at->format('d-m-Y')}}</div>
                </div>
            </div>
            </div>
        </div>
    </div>

@endsection
