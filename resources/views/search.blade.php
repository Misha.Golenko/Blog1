@extends('layouts.layout')
@section('content')
    <h3 style="text-align: center">Результат поиска "{{$request_search}}"</h3>
    <div class="row">
        @foreach($search_articles as $article)
                <a class="ajaxLoad" href="{{action('ArticleViewController@index',['id'=>$article->id])}}">
                <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img style="width: 100%; height: auto" class="" src='{{asset("images/$article->image")}}'>
                    </div>
                    <div class="panel-body">
                        <h4>{{$article->title}}</h4>
                    </div>
                    <div class="panel-body">
                        <p>{{$article->description}}</p>
                    </div>
                    <div class="panel-body">
                        <p>{{$article->created_at->format('d-m-Y')}}</p>
                    </div>
                </div>
            </div>
                </a>
        @endforeach
    </div>
    <div class="col-md-offset-4">{{$search_articles->links()}}</div>

@endsection
