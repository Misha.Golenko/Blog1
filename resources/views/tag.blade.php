@extends('layouts.layout')
@section('content')
    <h3 style="text-align: center">{{$tag_this->name}}</h3>
    <div class="row">
        @foreach($tag_this_articles as $tag_this_article)
            <a class="ajaxLoad" href="{{action('ArticleViewController@index',['id'=>$tag_this_article->id])}}">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img style="width: 100%; height: auto" class="" src='{{asset("images/$tag_this_article->image")}}'>
                    </div>
                    <div class="panel-body">
                        <h4>{{$tag_this_article->title}}</h4>
                    </div>
                    <div class="panel-body">
                        <p>{{$tag_this_article->description}}</p>
                    </div>
                    <div class="panel-body">
                        <p>{{$tag_this_article->created_at->format('d-m-Y')}}</p>
                    </div>
                </div>
            </div>
            </a>
        @endforeach
    </div>
    <div class="col-md-offset-4">{{$tag_this_articles->links()}}</div>

@endsection
